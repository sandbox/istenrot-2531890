Deduplicate
===========

INTRO
-----

This Drupal 7 module tries to save disk space on a server by
deduplicating (removing duplicate copies of a same file) files
uploaded to Drupal. It compares SHA-1 hashes of files. When
the same hash is found it unlinks duplicate copies and creates hard
links in place of unlinked files. Hard links are file system provided
feature which makes possible to link two identical files together in
a way that even though you see two separate files on your server the
data in files is actually stored only once on the logical storage
blocks. Additional copies won't take disk space when all copies are
linked together. This is the goal of this module.

For example you can use this module if you have a staging version
of a Drupal site hosted on the same server as production version.
This module will reduce disk space usage consumed by large image
files by deduplicating redundant copies away from staging sites.

FEATURES
--------

* Automatic deduplication when new files are uploaded to Drupal
* Deduplication of existing files

INSTALLING
----------

1) Download the module to sites/all/modules.
2) Create a new directory for dedupliation storage
   * By default the directory is sites/all/files
     and it works if all sites are under a single core
   * You can also have the directory outside Drupal root
     for instance /var/local/deduplication
   * Enable write access to the directory for the web server
     or php-fpm process, what ever applies to your hosting setup
3) Enable the module
4) Configure deduplication storage path to module config page
5) Enable automatic deduplication

DEDUPLICATING EXISTING FILES
----------------------------
First you need to configure deduplication storage path to the module
configuration settings. Then you can start deduplication of existing
files from "Run deduplication" tab on the configuration page.


